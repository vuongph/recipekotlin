package tvo.example.recipeapp.component

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.cpn_spinner.view.*
import tvo.example.recipeapp.R
import java.util.*

/**
 * Created By VuongPH on 4/2/2021
 */
class CpnSpinner : FrameLayout {
    var hint: String? = null
    var gravity = 0
    var textSize = 0f
    var hideHintWhenDropDown = false

    constructor(context: Context) :super(context){
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }
    constructor(context: Context,attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init(context, attrs)
    }

    @SuppressLint("CustomViewStyleable")
    private fun init(
        context: Context,
        attrs: AttributeSet?
    ) {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.cpn_spinner, this, true)
        if (attrs != null) {
            val typedArray =
                context.obtainStyledAttributes(attrs, R.styleable.cpn_spinner, 0, 0)
            setEnabled(typedArray.getBoolean(R.styleable.cpn_spinner_android_enabled, true))
            hint = typedArray.getString(R.styleable.cpn_spinner_android_hint)
            textSize = typedArray.getDimensionPixelSize(
                R.styleable.cpn_spinner_textSize,
                resources.getDimension(R.dimen._6ssp).toInt()
            ).toFloat()
            gravity = typedArray.getInt(R.styleable.cpn_spinner_android_gravity, Gravity.START)
            hideHintWhenDropDown =
                typedArray.getBoolean(R.styleable.cpn_spinner_hideHintWhenDropDown, false)

            // todo apply more styling
            typedArray.recycle()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        spinner!!.isEnabled = enabled
        spinnerFrame!!.isEnabled = enabled
        imageDropDownIcon?.setColorFilter(R.color.color_drop_down_icon)
    }


    fun <T> setData(list: List<T?>, selection: Int) {
        val optimizeList = optimizeList(list)
        val adapter = SpinnerAdapter(
            context,
            optimizeList,
            !TextUtils.isEmpty(hint),
            hideHintWhenDropDown,
            gravity,
            textSize
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = adapter
        spinner!!.setSelection(selection)
    }

    fun setSelection(position: Int) {
        spinner!!.setSelection(position)
    }

    fun <T> setData(list: List<T?>) {
        setData(list, 0)
    }

    fun setSpinnerChangeListener(listener: AdapterView.OnItemSelectedListener?) {
        spinner!!.onItemSelectedListener = listener
    }

    private fun <T> optimizeList(list: List<T?>): List<T?> {
        if (!TextUtils.isEmpty(hint)) {
            val resultList: MutableList<T?> = ArrayList()
            resultList.add(hint as T?)
            resultList.addAll(list)
            return resultList
        }
        return list
    }

    internal class SpinnerAdapter<T>(
        context: Context?,
        private val listData: List<T>,
        private val useHint: Boolean,
        private val hideHintWhenDropDown: Boolean,
        private val gravity: Int,
        private val textSize: Float
    ) :
        ArrayAdapter<T>(context!!, R.layout.item_spinner, listData) {
        private val inflater: LayoutInflater = LayoutInflater.from(context)
        private val layoutRes: Int = R.layout.item_spinner
        override fun getDropDownView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View {
            return if (position == 0 && useHint && hideHintWhenDropDown) {
                val v = TextView(context)
                v.height = 0
                v.visibility = View.GONE
                v
            } else {
                createItemView(position, convertView, parent, true)
            }
        }

        override fun getView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View {
            return createItemView(position, convertView, parent, false)
        }

        private fun createItemView(
            position: Int,
            convertView: View?,
            parent: ViewGroup,
            isDropDown: Boolean
        ): View {
            val view = inflater.inflate(layoutRes, parent, false)
            val textView = view.findViewById<TextView>(R.id.tvValue)
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
            textView.text = listData[position].toString()
            textView.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (useHint && position == 0) R.color.color_gray else R.color.color_back
                )
            )
            if (!isDropDown) {
                textView.gravity = gravity
            }
            return view
        }

    }

    abstract class SimpleOnItemSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View,
            position: Int,
            id: Long
        ) {
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }
}