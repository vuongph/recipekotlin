package tvo.example.recipeapp.model

/**
 * Created By VuongPH on 4/2/2021
 */
class RecipeType() {
    var id: Long = 0
    var type: String? = null

    override fun toString(): String {
        return type.toString()
    }
}