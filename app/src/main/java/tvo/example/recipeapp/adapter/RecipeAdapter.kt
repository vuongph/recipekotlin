package tvo.example.recipeapp.adapter

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recipe_item.view.*
import tvo.example.recipeapp.R
import tvo.example.recipeapp.base.BaseViewHolder
import tvo.example.recipeapp.database.table.Recipe
import tvo.example.recipeapp.helper.ImageHelper

/**
 * Created By VuongPH on 4/2/2021
 */
open class RecipeAdapter(context: Context?, items: MutableList<Recipe>?) :
    BaseAdapter<Recipe, RecipeAdapter.ViewHolder>(context, items) {

    class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

    }

    override fun getLayoutId(): Int {
        return R.layout.recipe_item
    }

    override fun createViewHolder(view: View?): ViewHolder {
        return view?.let {
            ViewHolder(
                it
            )
        }!!
    }

    override fun bindView(holder: BaseViewHolder, item: Recipe, position: Int) {
        ImageHelper.loadImage(
            holder.itemView.imgRecipe,
            item.imageUrl,
            R.drawable.img_empty_default,
            R.drawable.img_empty_default
        )
        holder.itemView.tvRecipeType.text = item.typeName
        holder.itemView.tvRecipeName.text = item.name
    }
}