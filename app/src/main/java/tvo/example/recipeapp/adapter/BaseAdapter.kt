package tvo.example.recipeapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONException
import tvo.example.recipeapp.base.BaseViewHolder
import java.util.*

/**
 * Created By VuongPH on 4/2/2021
 */
abstract class BaseAdapter<T, VH: BaseViewHolder> : RecyclerView.Adapter<VH> {
    private var mContext: Context? = null
    private var mInflater: LayoutInflater? = null
    private var mDisplayItems: MutableList<T>? = null
    var mListener: OnItemClickListener? = null

    protected abstract fun getLayoutId(): Int

    protected abstract fun createViewHolder(view: View?): VH

    constructor()

    constructor(context: Context?, items: MutableList<T>?){
        mContext = context
        mDisplayItems = items
        if (mDisplayItems == null) {
            mDisplayItems = ArrayList()
        }
        mInflater = LayoutInflater.from(context)
    }


    @Throws(JSONException::class)
    protected abstract fun bindView(holder: BaseViewHolder, item: T, position: Int)


    interface OnItemClickListener {
        fun onItemClick(
            adapter: BaseAdapter<*, *>?,
            view: View?,
            position: Int
        )
    }


    fun setOnItemClickListener(listener: OnItemClickListener?) {
        mListener = listener
    }

    fun getContext(): Context? {
        return mContext
    }

    fun getLayoutInflater(): LayoutInflater? {
        return mInflater
    }

    fun getDisplayItems(): List<T>? {
        return mDisplayItems
    }

    open fun getListener(): OnItemClickListener? {
        return mListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = getLayoutInflater()!!.inflate(getLayoutId(), parent, false)
        return createViewHolder(view)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = getItemAtPosition(position)
        try {
            bindView(holder as BaseViewHolder, item, position)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        holder.itemView.setOnClickListener { view ->
            onItemClick(holder as BaseViewHolder, position)
            if (mListener != null) {
                mListener!!.onItemClick(this@BaseAdapter, view, position)
            }
        }
    }

    override fun getItemCount(): Int {
        return mDisplayItems?.size ?: 0
    }

    fun getItemAtPosition(position: Int): T {
        return mDisplayItems!![position]
    }

    protected open fun onItemClick(holder: BaseViewHolder, position: Int) {}

    fun addData(items: List<T>?) {
        if (mDisplayItems == null) {
            mDisplayItems = ArrayList()
        }
        mDisplayItems!!.addAll(items!!)
        notifyDataSetChanged()
    }

    open fun updateData(items: List<T>?) {
        if (mDisplayItems == null) {
            mDisplayItems = ArrayList()
        }
        mDisplayItems!!.clear()
        mDisplayItems!!.addAll(items!!)
        notifyDataSetChanged()
    }
}