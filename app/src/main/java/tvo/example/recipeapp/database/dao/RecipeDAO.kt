package tvo.example.recipeapp.database.dao

import androidx.room.Query
import tvo.example.recipeapp.database.table.Recipe
import tvo.example.recipeapp.database.table.Table

/**
 * Created By VuongPH on 4/2/2021
 */
@androidx.room.Dao
interface RecipeDAO : DAO<Recipe> {
    @Query(" SELECT * FROM " + Table.RECIPE + " WHERE typeId == :typeId")
    fun getRecipes(typeId: Long): MutableList<Recipe>?
}