package tvo.example.recipeapp.database.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created By VuongPH on 4/1/2021
 */
@Entity(tableName = "step")
data class Step(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name = "recipeId")
    val recipeId: Long,

    @ColumnInfo(name = "stepDescription")
    val stepDescription: String?
)
