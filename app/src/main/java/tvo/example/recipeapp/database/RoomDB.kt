package tvo.example.recipeapp.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import tvo.example.recipeapp.App
import tvo.example.recipeapp.database.dao.RecipeDAO
import tvo.example.recipeapp.database.table.Recipe
import tvo.example.recipeapp.database.table.Step

@Database(entities = [Recipe::class, Step::class], version = 2)
abstract class RoomDB : RoomDatabase() {
    companion object {
        val instance: RoomDB by lazy {
            Room.databaseBuilder(App.context, RoomDB::class.java, "RecipeDatabase")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun recipeDao(): RecipeDAO

}