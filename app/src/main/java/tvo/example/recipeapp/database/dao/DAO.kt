package tvo.example.recipeapp.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

/**
 * Created By VuongPH on 4/2/2021
 */
interface DAO<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg t: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(t: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAndUpdate(t: T)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertNotReplace(t: T)

    @Update
    fun update(vararg t: T)

    @Update
    fun update(t: T)

    @Delete
    fun delete(vararg t: T)

    @Delete
    fun delete(t: T)
}