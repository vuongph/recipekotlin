package tvo.example.recipeapp.database

/**
 * Created By VuongPH on 4/2/2021
 */
interface ICompletable {
    fun onSuccess()

    fun onError(throwable: Throwable?)
}