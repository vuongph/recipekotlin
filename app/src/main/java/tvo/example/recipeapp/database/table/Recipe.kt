package tvo.example.recipeapp.database.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created By VuongPH on 4/1/2021
 */
@Entity(tableName = Table.RECIPE)
data class Recipe(
    @ColumnInfo(name = "imageUrl")
    val imageUrl: String?,

    @ColumnInfo(name = "typeId")
    val typeId: Long = 0L,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "typeName")
    val typeName: String?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
}
