package tvo.example.recipeapp.base.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import java.util.*

/**
 * Created By VuongPH on 4/2/2021
 */
abstract class BaseDialog : DialogFragment() {
    var isShowing = false

    protected abstract fun getDialogLayout(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getDialogLayout(), container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        Objects.requireNonNull(dialog.window)
            ?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (isShowing) return

        super.show(manager, tag)
        isShowing = true
    }

    override fun onDismiss(dialog: DialogInterface) {
        isShowing = false
        super.onDismiss(dialog)
    }

    protected fun setupDialog(width: Int, height: Int) {
        if (getDialog() == null || getDialog()?.getWindow() == null) {
            return
        }
        //set dialog size
        getDialog()?.getWindow()?.setLayout(
            width,
            height
        )
    }
}