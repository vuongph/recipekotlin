package tvo.example.recipeapp.base.rx

import io.reactivex.Flowable
import org.json.JSONException
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription


/**
 * Created By VuongPH on 4/2/2021
 */
abstract class Request<U> : Subscriber<U> {
    override fun onNext(response: U?) {
        //TODO:
    }

    override fun onError(throwable: Throwable?) {
        throwable?.printStackTrace()
    }

    override fun onComplete() {
        //TODO:
    }

    override fun onSubscribe(s: Subscription?) {
        //TODO:
    }

    abstract fun getRequest(): Flowable<U?>?

    @Throws(JSONException::class)
    abstract fun handleResponse(response: U?)

    abstract fun handleFailed(errCode: String?, errMessage: String?)

    abstract fun handleError(errCode: Int, throwable: Throwable?)

}