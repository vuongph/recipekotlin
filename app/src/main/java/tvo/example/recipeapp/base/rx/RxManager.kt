package tvo.example.recipeapp.base.rx

import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import tvo.example.recipeapp.database.ICompletable

/**
 * Created By VuongPH on 4/2/2021
 */
class RxManager {
    private var disposables: CompositeDisposable? = null

    constructor() {
        disposables = CompositeDisposable()
    }


    fun getDisposables(): CompositeDisposable? {
        return disposables
    }

    fun <T> execute(request: Request<T>) {
        val disposable = request.getRequest()
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.doOnError { throwable -> throwable.printStackTrace() }
            ?.subscribe({ t -> request.handleResponse(t) }, request::onError)
        disposable?.let { disposables!!.add(it) }
    }


    fun run(action: Completable, listener: ICompletable?) {
        val disposable = action
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(listener!!::onSuccess, listener!!::onError)
        disposables!!.add(disposable)
    }
}
