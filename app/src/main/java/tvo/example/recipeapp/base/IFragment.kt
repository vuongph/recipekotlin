package tvo.example.recipeapp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.x.y.z.smpl.base.InflateHelper
import tvo.example.recipeapp.base.rx.RxManager

abstract class IFragment<VM : IViewModel> : Fragment() {
    open val rxManager = RxManager()
    open val viewInflate: ViewInflate by lazy { InflateHelper.getAnnotation(this) }
    open val viewModel: VM by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(viewInflate.viewModel.java) as VM
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (viewInflate.layout != ResourcesCompat.ID_NULL) {
            return inflater.inflate(viewInflate.layout, container, false)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(savedInstanceState)
    }

    open fun initView(savedInstanceState: Bundle?) {
        onViewListener()
        onViewReady(savedInstanceState)
    }

    abstract fun onViewReady(savedInstanceState: Bundle?)

    open fun onViewListener() {
        // free implement
    }
}