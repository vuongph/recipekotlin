package tvo.example.recipeapp.base.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import tvo.example.recipeapp.R
import java.util.*

/**
 * Created By VuongPH on 4/2/2021
 */
class ProgressDialog(context: Context) : Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Objects.requireNonNull(window)!!.setBackgroundDrawable(ColorDrawable(0))
        @SuppressLint("InflateParams") val view: View =
            LayoutInflater.from(this.context).inflate(R.layout.dialog_progress, null)
        setContentView(view)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
    }
}