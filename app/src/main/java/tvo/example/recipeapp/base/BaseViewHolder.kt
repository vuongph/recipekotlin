package tvo.example.recipeapp.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created By VuongPH on 4/2/2021
 */
abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)