package com.x.y.z.smpl.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import tvo.example.recipeapp.base.IViewModel
import tvo.example.recipeapp.base.ViewInflate

abstract class IActivity<VM : IViewModel> : AppCompatActivity() {
    open val viewInflate: ViewInflate by lazy { InflateHelper.getAnnotation(this) }
    open val viewModel: VM by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(viewInflate.viewModel.java) as VM
    }


    final override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView(savedInstanceState)
    }

    open fun initView(savedInstanceState: Bundle?) {
        if (viewInflate.layout != ResourcesCompat.ID_NULL) {
            setContentView(viewInflate.layout)
        }

        onViewListener()
        onViewReady(savedInstanceState)
    }

    abstract fun onViewReady(savedInstanceState: Bundle?)

    open fun onViewListener() {
        // free implement
    }
}