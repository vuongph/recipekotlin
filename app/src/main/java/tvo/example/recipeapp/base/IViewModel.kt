package tvo.example.recipeapp.base

import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import tvo.example.recipeapp.base.rx.RxManager
import tvo.example.recipeapp.database.dao.DAO
import tvo.example.recipeapp.database.ICompletable

open class IViewModel() : ViewModel() {
    val rxManager: RxManager = RxManager()
    protected open fun <T> insertDB(
        `object`: T,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(
            Completable.fromAction { query.insert(`object`) },
            listener
        )
    }

    protected open fun <T> insertDBNotReplace(
        `object`: T,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(Completable.fromAction {
            query.insertNotReplace(
                `object`
            )
        }, listener)
    }

    protected open fun <T> insertDB(
        `object`: List<T>,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(Completable.fromAction {
            for (model in `object`) {
                query.insert(model)
            }
        }, listener)
    }

    protected open fun <T> insertAndUpdate(
        `object`: T,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(Completable.fromAction {
            query.insertAndUpdate(
                `object`
            )
        }, listener)
    }

    protected open fun <T> insertAndUpdate(
        `object`: List<T>,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(Completable.fromAction {
            for (model in `object`) {
                query.insertAndUpdate(model)
            }
        }, listener)
    }


    protected open fun <T> updateDB(
        `object`: T,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(
            Completable.fromAction { query.update(`object`) },
            listener
        )
    }

    protected open fun <T> updateDB(
        `object`: List<T>,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(Completable.fromAction {
            for (baseModel in `object`) {
                query.update(baseModel)
            }
        }, listener)
    }

    protected open fun <T> deleteDB(
        `object`: T,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(
            Completable.fromAction { query.delete(`object`) },
            listener
        )
    }

    protected open fun <T> deleteDB(
        `object`: List<T>,
        query: DAO<T>,
        listener: ICompletable?
    ) {
        rxManager.run(Completable.fromAction {
            for (baseModel in `object`) {
                query.delete(baseModel)
            }
        }, listener)
    }
}