package tvo.example.recipeapp.base

import androidx.annotation.LayoutRes
import androidx.core.content.res.ResourcesCompat
import kotlin.reflect.KClass

@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class ViewInflate(
    @LayoutRes val layout: Int = ResourcesCompat.ID_NULL,
    val viewModel: KClass<out IViewModel> = IViewModel::class
)