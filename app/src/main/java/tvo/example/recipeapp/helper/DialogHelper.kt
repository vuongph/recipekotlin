package tvo.example.recipeapp.helper

import android.content.Context
import tvo.example.recipeapp.base.dialog.ProgressDialog

/**
 * Created By VuongPH on 4/2/2021
 */
class DialogHelper {
    companion object {
        //    private static ProgressDialog mProgressDialog;
        private var progressDialog: ProgressDialog? = null
        private var mProgressCounter = 0

        @JvmStatic
        fun showProgress(context: Context?) {
            if (progressDialog == null) {
                mProgressCounter = 0
                progressDialog = context?.let { ProgressDialog(it) }
            } else {
                mProgressCounter++
            }
            if (!progressDialog?.isShowing!!) {
                progressDialog?.show()
            }
        }

        @JvmStatic
        fun hideProgress() {
            if (progressDialog != null) {
                mProgressCounter--
                if (mProgressCounter <= 0) {
                    mProgressCounter = 0
                    progressDialog!!.dismiss()
                    progressDialog = null
                }
            }
        }
    }

}