package tvo.example.recipeapp.helper

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created By VuongPH on 4/2/2021
 */
class ImageHelper {
    companion object{
        fun <T> loadImage(
            imageView: ImageView?,
            data: T,
            placeHolder: Int,
            errorHolder: Int
        ) {
            Glide.with(imageView!!)
                .load(data)
                .apply(RequestOptions().placeholder(placeHolder).error(errorHolder))
                .into(imageView)
        }
    }
}