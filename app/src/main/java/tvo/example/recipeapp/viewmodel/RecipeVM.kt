package tvo.example.recipeapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tvo.example.recipeapp.database.RoomDB
import io.reactivex.Flowable
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import tvo.example.recipeapp.App.Companion.context
import tvo.example.recipeapp.base.IViewModel
import tvo.example.recipeapp.base.rx.Request
import tvo.example.recipeapp.database.ICompletable
import tvo.example.recipeapp.database.table.Recipe
import tvo.example.recipeapp.model.RecipeType
import java.io.IOException
import java.io.InputStream


/**
 * Created By VuongPH on 4/2/2021
 */
class RecipeVM : IViewModel() {
    private val mldRecipeTypes = MutableLiveData<MutableList<RecipeType>>()
    private val mldRecipes = MutableLiveData<MutableList<Recipe>>()
    fun getRecipeTypesLd(): LiveData<MutableList<RecipeType>>? {
        return mldRecipeTypes
    }

    fun getRecipeLd(): LiveData<MutableList<Recipe>>? {
        return mldRecipes
    }

    fun getRecipeTypes() {
        rxManager.execute(object : Request<MutableList<RecipeType>>() {
            override fun getRequest(): Flowable<MutableList<RecipeType>?>? {
                return Flowable.just(getRecipeTypesFromXml()).map { t -> t }
            }

            override fun handleResponse(response: MutableList<RecipeType>?) {
                mldRecipeTypes.value = response
                response?.get(0)?.id?.let { getRecipeByType(it) }
            }

            override fun handleFailed(errCode: String?, errMessage: String?) {
                TODO("Not yet implemented")
            }

            override fun handleError(errCode: Int, throwable: Throwable?) {
                TODO("Not yet implemented")
            }

        })
    }

    fun getRecipeByType(typeId: Long): MutableList<Recipe> {
        return RoomDB.instance.recipeDao().getRecipes(typeId)!!
    }

    fun createRecipe() {
        val recipe = fakeData()
        insertAndUpdate(recipe, RoomDB.instance.recipeDao(), object : ICompletable {
            override fun onSuccess() {
                mldRecipes.value?.add(recipe)
            }

            override fun onError(throwable: Throwable?) {
                TODO("Not yet implemented")
            }

        })
    }

    private fun fakeData(): Recipe {
        return Recipe( "https://hatrungdung.com/wp-content/uploads/2018/04/cong-thuc-che-bien-mon-ca-cuon-tom-trung-cut-cuc-ki-ngon-mieng-675x450.jpg", 1, "Test1","typeTEst")
    }

    private fun getRecipeTypesFromXml(): MutableList<RecipeType> {
        val parserFactory: XmlPullParserFactory
        return try {
            parserFactory = XmlPullParserFactory.newInstance()
            val parser = parserFactory.newPullParser()
            val `is`: InputStream = context.assets.open("recipetypes.xml")
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(`is`, null)
            processParsing(parser)
        } catch (e: XmlPullParserException) {
            ArrayList()
        } catch (e: IOException) {
            ArrayList()
        }
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun processParsing(parser: XmlPullParser): MutableList<RecipeType> {
        val recipeTypes: MutableList<RecipeType> = mutableListOf()
        var recipeType = parser.eventType
        var currentRecipeType: RecipeType? = null
        while (recipeType != XmlPullParser.END_DOCUMENT) {
            var eltName: String? = null
            when (recipeType) {
                XmlPullParser.START_TAG -> {
                    eltName = parser.name
                    if ("type" == eltName) {
                        currentRecipeType = RecipeType()
                        recipeTypes.add(currentRecipeType)
                    } else if (currentRecipeType != null) {
                        when (eltName) {
                            "id" -> currentRecipeType.id = parser.nextText().toLong()
                            "name" -> currentRecipeType.type = parser.nextText()

                        }
                    }
                }
            }
            recipeType = parser.next()
        }
        return recipeTypes
    }
}