package tvo.example.recipeapp.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_recipe_list.*
import tvo.example.recipeapp.R
import tvo.example.recipeapp.adapter.RecipeAdapter
import tvo.example.recipeapp.base.IFragment
import tvo.example.recipeapp.base.ViewInflate
import tvo.example.recipeapp.component.CpnSpinner
import tvo.example.recipeapp.database.table.Recipe
import tvo.example.recipeapp.viewmodel.RecipeVM

@Suppress("UNREACHABLE_CODE")
@SuppressLint("NonConstantResourceId")
@ViewInflate(layout = R.layout.fragment_recipe_list, viewModel = RecipeVM::class)
class RecipeListFragment : IFragment<RecipeVM>() {
    override fun onViewReady(savedInstanceState: Bundle?) {
        initLiveData()
        btnAddRecipe.setOnClickListener { viewModel.createRecipe() }
    }

    private fun initLiveData() {
        viewModel.getRecipeTypesLd()?.observe(viewLifecycleOwner, Observer { response ->
            run {
                spnRecipeType.setData(response)
                spnRecipeType.setSelection(0)
                spnRecipeType.setSpinnerChangeListener(object :
                    CpnSpinner.SimpleOnItemSelectedListener() {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        if (position > 0) {
                            initRecyclerView(viewModel.getRecipeByType(response[position-1].id))
                        }
                    }
                })
            }
        })
        viewModel.getRecipeTypes()
    }

    private fun initRecyclerView(items: MutableList<Recipe>) {
        rcvRecipe.adapter = RecipeAdapter(context, items)
    }
}